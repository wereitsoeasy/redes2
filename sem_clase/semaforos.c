#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <sys/types.h> 
#include <sys/ipc.h> 
#include <sys/sem.h>
#include <pthread.h>


int id, secc_critica;

int main() {
  //paso 0: cracion de una llave, le vamos a pasar el path a el comando ls
  yabesita = ftok("/bin/ls", 1);
  if(yabesita < 0){
    printf("error");
    exit(1); //algo diferente de cero significa un error
  }
  //paso 1. creacion de los semaforos
  id = semget(yabesita, 2, 0777|IPC_CREAT|IPC_EXCL);
  if(id < 0){
    printf("Error en semget");
    exit(1);
  }
  //paso 2. inicializar los semaforos
  semctl(id, 0, SETVAL, 1); //para productor
  semctl(id, 1, SETVAL, 0); //para consumidor
  //paso 3 , crear hilo producto y el hilo consumidor
  pthread_t productor, consumidor;
  pthread_create(&productor, NULL, func_prod, NULL);
  pthread_create(&consumidor, NULL, func_consu, NULL);

  pthread_join(productor, NULL);
  pthread_join(consumidor, NULL);
  
}

void func_prod(void* args){
  struct sembuf oper;
  oper.sem_num = 0; //numero de semaforo que se va a utilizar
  oper.sem_op = -1; //se decrementa por que lo primero que se hace es un wait
  oper.sem_flag = SEM_UNDO;
  semop(id, &sembuf, 1);
  int i;
  for(i = 0; i < 10; i++){
    secc_critica=i;
    printf("productor %d", secc_criica);
  }
  oper.sem_num = 1; //numero de semaforo que se va a utilizar
  oper.sem_op = 1; //se decrementa por que lo primero que se hace es un wait
  oper.sem_flag = SEM_UNDO;
  semop(id, &sembuf, 1);
}

void func_consu(void* args){
  struct sembuf oper;
  oper.sem_num = 1; //numero de semaforo que se va a utilizar
  oper.sem_op = -1; //se decrementa por que lo primero que se hace es un wait
  oper.sem_flag = SEM_UNDO;
  int i;
  for(i = 0; i < 10; i++){
    printf("consumidor: %d", secc_critica);
  }
  oper.sem_num = 0; //numero de semaforo que se va a utilizar
  oper.sem_op = 1; //se decrementa por que lo primero que se hace es un wait
  oper.sem_flag = SEM_UNDO;
  semop(id, &sembuf, 1);
}
