/*
  tabla de multiplicar. cada uno de los hilos va a hacer una tabla de multiplicar. hacer esto por n tablas
*/

#include <stdio.h>
#include <pthread.h>

void* f_hilo(void* params){
  int *int_params=(int *)params;
  //pthread_exit(NULL); Se elimina debido a: 
  /* If the
     start_routine returns, the effect is as if there was an implicit call to pthread_exit() using
     the return value ofstart_routine as the exit status.*/
  int i;
  printf("tabla del %d: ", *int_params);
  for(i = 1; i<11; i++){
    printf("%d ", (*int_params)*i);
  }
  printf("\n");
  return NULL;
}
  
int main(){
  pthread_t hilo[10];
  int i;
  int j[12];
  for(i=0;i<10;i++){
    j[i] = i;
    pthread_create(&hilo[i], NULL, f_hilo, (void *) &j[i]);
  }
  void **aux;
  for(i=0;i<10;i++){
    pthread_join(hilo[i], aux);
  }
  return 0;
}
