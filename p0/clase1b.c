//version 2

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

void* f_hilo(void* params){
  int *int_params=(int *)params;
  int i;
  static int *ptr;
  ptr = (int*)malloc(10*sizeof(int));
  for(i = 0; i<10; i++){
    *(ptr+i)=(*int_params)*(i+1);
  }
  return (void*)ptr;
}
  
int main(){
  pthread_t hilo[10];
  int i,k;
  int j[12];
  for(i=0;i<10;i++){
    j[i]=i;
    pthread_create(&hilo[i], NULL, f_hilo, (void *)&j[i]);
  }
  int **t_fetch;
  for(i=0;i<10;i++){
    pthread_join(hilo[i], (void**)t_fetch);
    printf("tabla del %d: ", (*t_fetch)[0]);
    for(k=0;k<10;k++){
      printf("%d ", (*t_fetch)[k]);
    }
    printf("\n");
  }
  return 0;
}
